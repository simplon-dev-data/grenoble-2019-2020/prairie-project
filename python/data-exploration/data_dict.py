import math

from datetime import date, timedelta


promo: list = [
    {"id": 1, "nom": "Boukachab", "prenom": "Nolan", "date-de-naissance": "1992-02-22", "latitude": 45.399, "longitude": 5.672, "temps-trajet": 90},
    {"id": 2, "nom": "Paradis", "prenom": "Adrien", "date-de-naissance": "1987-01-20", "latitude": 45.1445281, "longitude": 5.7280384, "temps-trajet": 15},
    {"id": 3, "nom": "Chambat", "prenom": "Alexis", "date-de-naissance": "1991-08-06", "latitude": 45.4, "longitude": 6.0667, "temps-trajet": 90},
    {"id": 4, "nom": "Atta", "prenom": "Khalid", "date-de-naissance": "1976-12-25", "latitude": 45.1667, "longitude": 5.7167, "temps-trajet": 12},
    {"id": 5, "nom": "Stacchetti", "prenom": "Julien", "date-de-naissance": "1979-02-09", "latitude": 45.1788258, "longitude": 5.74639579999996, "temps-trajet": 35},
    {"id": 6, "nom": "Pieczara", "prenom": "Victor", "date-de-naissance": "1988-03-30", "latitude": 45.494761, "longitude": 5.139048, "temps-trajet": 80},
    {"id": 7, "nom": "Kosnar", "prenom": "Jean-Charles", "date-de-naissance": "1984-05-15", "latitude": 45.0759, "longitude": 5.77018, "temps-trajet": 45},
    {"id": 8, "nom": "Azarual", "prenom": "El Ghazi", "date-de-naissance": "1982-01-01", "latitude": 45.18756, "longitude": 5.735782, "temps-trajet": 40},
    {"id": 9, "nom": "Tatin", "prenom": "Dominique", "date-de-naissance": "1961-08-03", "latitude": 44.9901, "longitude": 5.62071, "temps-trajet": 45},
    {"id": 10, "nom": "Khaldi", "prenom": "Sofien", "date-de-naissance": "1992-02-10", "latitude": 45.1667, "longitude": 5.17167, "temps-trajet": 10},
    {"id": 11, "nom": "Bonnin", "prenom": "Alice", "date-de-naissance": "1985-05-17", "latitude": 45.392241, "longitude": 6.074780, "temps-trajet": 70},
    {"id": 12, "nom": "Dubeau", "prenom": "Zahra", "date-de-naissance": "1986-10-05", "latitude": 45.1667, "longitude": 5.7167, "temps-trajet": 30},
    {"id": 13, "nom": "Tsitoglou", "prenom": "Kévin", "date-de-naissance": "1992-01-17", "latitude": 45.4333, "longitude": 6.0167, "temps-trajet": 50},
    {"id": 14, "nom": "Saoudi", "prenom": "Toufik", "date-de-naissance": "1976-01-25", "latitude": 45.1439641, "longitude": 5.6799326, "temps-trajet": 8},
    {"id": 15, "nom": "Zouaoui", "prenom": "Rached", "date-de-naissance": "1994-01-08", "latitude": 45.2183701, "longitude": 5.7392462, "temps-trajet": 4},
    {"id": 16, "nom": "Berrahal", "prenom": "Hamid", "date-de-naissance": "1983-04-18", "latitude": 45.18756, "longitude": 5.735782, "temps-trajet": 20},
    {"id": 17, "nom": "Alshabeen", "prenom": "Abdulhadi", "date-de-naissance": "1990-11-15", "latitude": 45.197072, "longitude": 5.6754582, "temps-trajet": 1},
]


def average_age(data:list) -> int:
    today: date = date.today()
    age_sum: int = 0

    for item in data:
        birth_date: date = date.fromisoformat(item["date-de-naissance"])
        half_year = 1 if ((today.month, today.day) < (birth_date.month, birth_date.day)) else 0
        age_sum += today.year - birth_date.year - half_year

    return int(age_sum / len(data))


def order_by_name(data: list) -> list:
    def comparison_key(item):
        return (item["nom"], item["prenom"])

    return sorted(data, key=comparison_key)


def order_by_distance(data:list) -> list:
    origin = (45.1655775, 5.7279063)

    def distance_key(item):
        orig_lat_radians = origin[0] / 180.0
        orig_lng_radians = origin[1] / 180.0
        dest_lat_radians = item["latitude"] / 180.0
        dest_lng_radians = item["longitude"] / 180.0

        return 6378 * math.acos(
            math.sin(math.pi * orig_lat_radians)
            * math.sin(math.pi * dest_lat_radians)
            + math.cos(math.pi * orig_lat_radians)
            * math.cos(math.pi * dest_lat_radians)
            * math.cos(math.pi * orig_lng_radians - math.pi * dest_lng_radians)
        )

    return sorted(data, key=distance_key)


print(f"average age: {average_age(promo)}")
print(f"ordered by name: {order_by_name(promo)}")
print(f"ordered by distance: {order_by_distance(promo)}")
