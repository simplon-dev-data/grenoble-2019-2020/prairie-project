import math

from datetime import date, timedelta


promo: list = [
    (1, "Boukachab", "Nolan", "1992-02-22", 45.399, 5.672, 90),
    (2, "Paradis", "Adrien", "1987-01-20", 45.1445281, 5.7280384, 15),
    (3, "Chambat", "Alexis", "1991-08-06", 45.4, 6.0667, 90),
    (4, "Atta", "Khalid", "1976-12-25", 45.1667, 5.7167, 12),
    (5, "Stacchetti", "Julien", "1979-02-09", 45.1788258, 5.74639579999996, 35),
    (6, "Pieczara", "Victor", "1988-03-30", 45.494761, 5.139048, 80),
    (7, "Kosnar", "Jean-Charles", "1984-05-15", 45.0759, 5.77018, 45),
    (8, "Azarual", "El Ghazi", "1982-01-01", 45.18756, 5.735782, 40),
    (9, "Tatin", "Dominique", "1961-08-03", 44.9901, 5.62071, 45),
    (10, "Khaldi", "Sofien", "1992-02-10", 45.1667, 5.17167, 10),
    (11, "Bonnin", "Alice", "1985-05-17", 45.392241, 6.074780, 70),
    (12, "Dubeau", "Zahra", "1986-10-05", 45.1667, 5.7167, 30),
    (13, "Tsitoglou", "Kévin", "1992-01-17", 45.4333, 6.0167, 50),
    (14, "Saoudi", "Toufik", "1976-01-25", 45.1439641, 5.6799326, 8),
    (15, "Zouaoui", "Rached", "1994-01-08", 45.2183701, 5.7392462, 4),
    (16, "Berrahal", "Hamid", "1983-04-18", 45.18756, 5.735782, 20),
    (17, "Alshabeen", "Abdulhadi", "1990-11-15", 45.197072, 5.6754582, 18)
]


def average_age(data:list) -> int:
    today: date = date.today()
    age_sum: int = 0

    for item in data:
        birth_date: date = date.fromisoformat(item[3])
        half_year = 1 if ((today.month, today.day) < (birth_date.month, birth_date.day)) else 0
        age_sum += today.year - birth_date.year - half_year

    return int(age_sum / len(data))


def order_by_name(data: list) -> list:
    def comparison_key(item):
        return (item[1], item[2])

    return sorted(data, key=comparison_key)


def order_by_distance(data:list) -> list:
    origin = (45.1655775, 5.7279063)

    def distance_key(item):
        orig_lat_radians = origin[0] / 180.0
        orig_lng_radians = origin[1] / 180.0
        dest_lat_radians = item[4] / 180.0
        dest_lng_radians = item[5] / 180.0

        return 6378 * math.acos(
            math.sin(math.pi * orig_lat_radians)
            * math.sin(math.pi * dest_lat_radians)
            + math.cos(math.pi * orig_lat_radians)
            * math.cos(math.pi * dest_lat_radians)
            * math.cos(math.pi * orig_lng_radians - math.pi * dest_lng_radians)
        )

    return sorted(data, key=distance_key)


print(f"average age: {average_age(promo)}")
print(f"ordered by name: {order_by_name(promo)}")
print(f"ordered by distance: {order_by_distance(promo)}")
