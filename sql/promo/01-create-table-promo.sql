CREATE TABLE promo
(
    id INTEGER PRIMARY KEY NOT NULL,
    nom TEXT,
    prenom TEXT,
    date_de_naissance DATE,
    latitude FLOAT,
    longitude FLOAT,
    minutes_trajet INTEGER
)
;

