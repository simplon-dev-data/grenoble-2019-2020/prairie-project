INSERT INTO books (id, title, author, release_date)
VALUES
(1, "Stories of Your Life and others", "Ted Chiang", "2015-05-21"),
(2, "Exhalation: Stories", "Ted Chiang", "2019-05-07"),
(3, "Weapons of Math Destruction", "Cathy O'Neil", "2016-09-06");

