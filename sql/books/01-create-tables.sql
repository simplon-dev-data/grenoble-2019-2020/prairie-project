CREATE TABLE books
(
    id INTEGER,
    title TEXT,
    author TEXT,
    release_date DATE
);

